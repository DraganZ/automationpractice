package util;

import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configs {

    static private Configs _instance = null;

    private String PROPERTIES_FILE_PATH = Constants.RESOURCE_PATH + "automationpractice.properties";


    public String BASE_URL = null;
    public String USERNAME = null;
    public String PASSWORD = null;


    protected Configs() {
        try (InputStream input = new FileInputStream(PROPERTIES_FILE_PATH)) {
            Properties prop = new Properties();
            prop.load(input);

            BASE_URL = getMandatoryProperty(prop, "baseUrl");
            USERNAME = getMandatoryProperty(prop, "username");
            PASSWORD = getMandatoryProperty(prop, "password");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Configs getInstance() {
        if (_instance == null) {
            _instance = new Configs();
        }
        return _instance;
    }

    private String getMandatoryProperty(Properties prop, String propertyName) {
        String propertyValue = prop.getProperty(propertyName);
        Assert.assertNotNull(propertyValue, "property" + propertyName + "in automationpractice.properties file is mandatory.");
        return propertyValue;
    }
}


