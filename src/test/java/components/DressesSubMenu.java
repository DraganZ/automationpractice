package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.CasualDressesPage;
import pages.EveningDressesPage;
import pages.SummerDressesPage;

public class DressesSubMenu extends AbstractBaseComponent {

    private By CASUAL_DRESSES = By.linkText("Casual Dresses");
    private By EVENING_DRESSES = By.linkText("Evening Dresses");
    private By SUMMER_DRESSES = By.linkText("Summer Dresses");

    public DressesSubMenu(WebDriver driver) {
        super(driver);
    }

    public CasualDressesPage clickCasualDresses() {
        clickLink(CASUAL_DRESSES);
        return new CasualDressesPage(driver);
    }

    public EveningDressesPage clickEveningDresses() {
        clickLink(EVENING_DRESSES);
        return new EveningDressesPage(driver);
    }

    public SummerDressesPage clickSummerDresses() {
        clickLink(SUMMER_DRESSES);
        return new SummerDressesPage(driver);
    }
}
