package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractBaseComponent {

    protected WebDriver driver;
    protected WebDriverWait wait;

    public AbstractBaseComponent(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 40);
    }

    protected void clickLink(By by) {
        driver.findElement(by).click();
    }

    protected void sendKeys(By by, String text) {
        driver.findElement(by).sendKeys(text);
    }

    protected void hoverAndClickButton(By hoverElementBy, By clickElementBy) {
        wait.until(ExpectedConditions.elementToBeClickable(hoverElementBy));
        WebElement element = driver.findElement(hoverElementBy);
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();

        WebElement button=driver.findElement(clickElementBy);
        button.click();
    }

    protected void hoverOver (By by) {
        WebElement element = driver.findElement(by);
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }

    public String getTitle(){
        return driver.getTitle();
    }
}
