package components;

import org.openqa.selenium.*;
import pages.LoginPage;

public class CheckoutComponent extends AbstractBaseComponent {

    private By CHECKOUT_BUTTON = By.xpath("//*[contains(@class, 'button btn')]");
    private By SEND_TEXT_CHECKOUT = By.xpath("//textarea[@name='message']");
    private By TOS_BOX = By.xpath("//input[@id='cgv']");
    private By PURCHASE_SUCCESSFUL_WIRE = By.xpath("//p[@class='cheque-indent']");
    private By PURCHASE_SUCCESSFUL_CHECK = By.xpath("//p[@class='alert alert-success']");
    private By NAME_CREDENTIALS = By.xpath("//strong[normalize-space()='Pradeep Macharla']");
    public static By WIRE_PAYMENT = By.xpath("//a[@title='Pay by bank wire']");
    public static By CHECK_PAYMENT = By.xpath("//a[@title='Pay by check.']");

    public CheckoutComponent(WebDriver driver) {
        super(driver);
    }

    public LoginPage checkoutLogin() {
        clickLink(CHECKOUT_BUTTON);
        return new LoginPage(driver);
    }

    public CheckoutComponent sendTextCheckout(String text) {
        sendKeys(SEND_TEXT_CHECKOUT, text);
        return new CheckoutComponent(driver);
    }

    public CheckoutComponent checkoutProceed() {
        clickLink(CHECKOUT_BUTTON);
        return new CheckoutComponent(driver);
    }

    public CheckoutComponent checkTOSBox() {
        clickLink(TOS_BOX);
        return new CheckoutComponent(driver);
    }

    public CheckoutComponent paymentMethod(By by) {
        clickLink(by);
        return new CheckoutComponent(driver);
    }
    public String getTextWire(){
        return driver.findElement(PURCHASE_SUCCESSFUL_WIRE).getText();
    }

    public String getTextCheck(){
        return driver.findElement(PURCHASE_SUCCESSFUL_CHECK).getText();
    }

    public String getName() {
        return driver.findElement(NAME_CREDENTIALS).getText();
    }
}
