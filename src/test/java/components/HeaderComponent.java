package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.CartPage;
import pages.HomePage;
import pages.LoginPage;

public class HeaderComponent extends AbstractBaseComponent {

    private By LOGO = By.xpath("//*[@id='header_logo']/a/img");
    private By CART = By.xpath("//a[@title='View my shopping cart']");
    private By LOGIN_BUTTON = By.className("login");

    public HeaderComponent(WebDriver driver) {
        super(driver);
    }

    public HomePage clickHeaderLogo() {
        clickLink(LOGO);
        return new HomePage(driver);
    }

    public CartPage clickOnCart() {
        clickLink(CART);
        return new CartPage(driver);
    }

    public LoginPage clickSignInButton() {
        clickLink(LOGIN_BUTTON);
        return new LoginPage(driver);
    }
}
