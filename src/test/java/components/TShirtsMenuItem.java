package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.TShirtPage;

public class TShirtsMenuItem extends AbstractBaseComponent {

    private By T_SHIRTS_DROPDOWN = By.xpath("//div[@id='block_top_menu']/ul/li[3]/a");

    public TShirtsMenuItem(WebDriver driver) {
        super(driver);
    }

    public TShirtPage clickTShirt() {
        clickLink(T_SHIRTS_DROPDOWN);
        return new TShirtPage(driver);
    }
}
