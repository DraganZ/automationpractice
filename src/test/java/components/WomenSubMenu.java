package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.ProductPage;

public class WomenSubMenu extends AbstractBaseComponent {

    private By WOMEN_COMPONENT = By.xpath("//a[@title='Women']");

    public WomenSubMenu(WebDriver driver) {
        super(driver);
    }

    public ProductPage hoverAndClickWomenDropdown (String text) {
        hoverAndClickButton(WOMEN_COMPONENT, By.xpath("//a[@title='"+text+"']"));
        return new ProductPage(driver);
    }
}
