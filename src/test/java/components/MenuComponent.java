package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MenuComponent extends AbstractBaseComponent {

    private By WOMEN_DROPDOWN = By.xpath("//a[@title='Women']");
    private By DRESSES_DROPDOWN = By.xpath("//div[@id='block_top_menu']/ul/li[2]/a");

    protected WomenSubMenu womenSubMenu;
    protected DressesSubMenu dressesSubMenu;
    protected TShirtsMenuItem tShirtsMenuItem;

    public MenuComponent(WebDriver driver) {
        super(driver);
        this.womenSubMenu = new WomenSubMenu(driver);
        this.dressesSubMenu = new DressesSubMenu(driver);
        this.tShirtsMenuItem = new TShirtsMenuItem(driver);
    }

    public WomenSubMenu hoverOverWomenMenu() {
        hoverOver(WOMEN_DROPDOWN);
        return new WomenSubMenu(driver);
    }

    public DressesSubMenu hoverOverDresses() {
        hoverOver(DRESSES_DROPDOWN);
        return new DressesSubMenu(driver);
    }

    public TShirtsMenuItem getTShirtsMenuItem(){
        return tShirtsMenuItem;
    }
}
