package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected WebDriver driver;
    protected HomePage homePage;

    protected void createDriver() {
        System.setProperty("webdriver.chrome.driver", Constants.RESOURCE_PATH + "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    protected void quitDriver() {
        driver.quit();
    }

    protected void goHome() {
        driver.get("http://automationpractice.com/index.php");
        homePage = new HomePage(driver);
    }
}
