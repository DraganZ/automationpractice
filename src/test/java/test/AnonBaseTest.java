package test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class AnonBaseTest extends BaseTest {

    @BeforeTest
    public void setUp() {
        createDriver();
    }

    @BeforeMethod
    protected void beforeMethod() {
        goHome();
    }

  @AfterTest
    public void tearDown() {
      quitDriver();
    }
}
