package test;

import jdk.jfr.Description;
import org.testng.annotations.Test;
import pages.FailedLoginPage;
import pages.HomePage;
import util.Configs;

import static org.testng.Assert.*;

public class LoginTest extends AnonBaseTest {

    @Test
    @Description("Testing login with incorrect Password")
    public void testLoginIncorrectPassword() {
        FailedLoginPage failedLoginPage=homePage
                .getHeaderComponent()
                .clickSignInButton()
                .loginUnsuccessfully(Configs.getInstance().USERNAME, "password");
        assertTrue(failedLoginPage.getAlertText().contains("Authentication failed."),
                "Login with incorrect password failed");
    }

    @Test
    @Description("Testing login with incorrect Username")
    public void testLoginIncorrectUsername() {
        FailedLoginPage failedLoginPage=homePage
                .getHeaderComponent()
                .clickSignInButton()
                .loginUnsuccessfully("username@username.com", Configs.getInstance().PASSWORD);
        assertTrue(failedLoginPage.getAlertText().contains("Authentication failed."),
                "Login with incorrect username failed");
    }

    @Test
    @Description("Testing login with wrong credentials")
    public void testFailedLogin() {
        FailedLoginPage failedLoginPage=homePage
                .getHeaderComponent()
                .clickSignInButton()
                .loginUnsuccessfully("username@username.com", "password");
        assertTrue(failedLoginPage.getAlertText().contains("Authentication failed."),
                "Login with incorrect credentials failed");
    }

    @Test
    @Description("Testing successful login")
    public void testSuccessfulLogin() {
        HomePage homePagee = homePage
                .getHeaderComponent()
                .clickSignInButton()
                .loginSuccessfully(Configs.getInstance().USERNAME, Configs.getInstance().PASSWORD);
        assertEquals(homePagee.getTitle(), "My account - My Store",
                "Test is successfully executed");
    }
}