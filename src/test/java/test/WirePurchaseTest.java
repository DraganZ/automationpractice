package test;

import components.CheckoutComponent;
import jdk.jfr.Description;
import org.testng.annotations.Test;
import util.Configs;

import static org.testng.Assert.assertTrue;

public class WirePurchaseTest extends AnonBaseTest {

    @Test
    @Description("Testing, Login, Successful single item Check Purchase")
    public void checkPurchaseTest() {
        CheckoutComponent checkoutComponent=homePage.getMenu()
                .hoverOverWomenMenu()
                .hoverAndClickWomenDropdown("Blouses")
                .clickBlouse()
                .clickAndPickSize("M")
                .clickQuantity()
                .clickAddToCart()
                .clickCheckout()
                .checkoutLogin()
                .loginCheckout(Configs.getInstance().USERNAME, Configs.getInstance().PASSWORD)
                .sendTextCheckout("Random text")
                .checkoutProceed()
                .checkTOSBox()
                .checkoutProceed()
                .paymentMethod(CheckoutComponent.WIRE_PAYMENT)
                .checkoutProceed();
        assertTrue(checkoutComponent.getTextWire().contains("Your order on My Store is complete."), "Your order on My Store is not completed");
    }
}
