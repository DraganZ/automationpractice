package test;

import components.CheckoutComponent;
import jdk.jfr.Description;
import org.testng.annotations.Test;
import util.Configs;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckPurchaseTest extends AnonBaseTest{

    @Test
    @Description("Testing, Login, Check Purchase, and credential info")
    public void wirePurchaseTest() {
        CheckoutComponent checkoutComponent=homePage.getMenu()
                .hoverOverWomenMenu()
                .hoverAndClickWomenDropdown("T-shirts")
                .clickTShirt()
                .clickAndPickSize("L")
                .clickAddToCart()
                .clickCheckout()
                .checkoutLogin()
                .loginCheckout(Configs.getInstance().USERNAME, Configs.getInstance().PASSWORD)
                .sendTextCheckout("Single Item")
                .checkoutProceed()
                .checkTOSBox()
                .checkoutProceed()
                .paymentMethod(CheckoutComponent.CHECK_PAYMENT)
                .checkoutProceed();
        assertTrue(checkoutComponent.getTextCheck().contains("Your order on My Store is complete."), "Your order on My Store is not completed");
        assertEquals(checkoutComponent.getName(), "Pradeep Macharla", "Wrong Name");
    }
}

