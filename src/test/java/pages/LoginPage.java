package pages;

import components.CheckoutComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {

    private By LOGIN_BUTTON = By.xpath("//span[normalize-space()='Sign in']");
    private By USERNAME_FIELD = By.id("email");
    private By PASSWORD_FIELD = By.id("passwd");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void setUsername(String username) {
        sendKeys(USERNAME_FIELD, username);
    }

    public void setPassword(String password) {
        sendKeys(PASSWORD_FIELD, password);
    }

    public void clickLoginButton() {
        driver.findElement(LOGIN_BUTTON).click();
    }

    public HomePage loginSuccessfully(String username, String password) {
        setUsername(username);
        setPassword(password);
        clickLoginButton();
        return new HomePage(driver);
    }

    public CheckoutComponent loginCheckout(String username, String password) {
        setUsername(username);
        setPassword(password);
        clickLoginButton();
        return new CheckoutComponent(driver);
    }

    public FailedLoginPage loginUnsuccessfully(String username, String password) {
        setUsername(username);
        setPassword(password);
        clickLoginButton();
        return new FailedLoginPage(driver);
    }
}
