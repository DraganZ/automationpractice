package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DetailProductPage extends AbstractPage {

    private By SIZE_DROPDOWN = By.xpath("//div[@id='uniform-group_1']");
    private By CHOOSE_QUANTITY=By.xpath("//a[@class='btn btn-default button-plus product_quantity_up']//span");
    private By ADD_TO_CART=By.xpath("//span[normalize-space()='Add to cart']");


    public DetailProductPage(WebDriver driver) {
        super(driver);
    }

    public DetailProductPage clickAndPickSize(String size) {
        hoverAndClickButton(SIZE_DROPDOWN, By.xpath("//option[@title='"+size+"']"));
        return new DetailProductPage(driver);
    }

    public DetailProductPage clickQuantity() {
        clickLink(CHOOSE_QUANTITY);
        return new DetailProductPage(driver);
    }

    public CartPage clickAddToCart() {
        clickLink(ADD_TO_CART);
        return new CartPage(driver);
    }
}
