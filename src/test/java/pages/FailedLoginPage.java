package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FailedLoginPage extends AbstractPage {

    private By STATUS_ALERT = By.xpath("//li[normalize-space()='Authentication failed.']");

    public FailedLoginPage(WebDriver driver) {
        super(driver);
    }

    public String getAlertText() {
        return driver.findElement(STATUS_ALERT).getText();
    }
}

