package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage extends AbstractPage {

    private By BLOUSE_ITEM = By.xpath("//a[@title='Blouse'][normalize-space()='Blouse']");
    private By TSHIRT_ITEM = By.xpath("//a[normalize-space()='Faded Short Sleeve T-shirts']");

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public DetailProductPage clickBlouse() {
        clickLink(BLOUSE_ITEM);
        return new DetailProductPage(driver);
    }
    public DetailProductPage clickTShirt() {
        clickLink(TSHIRT_ITEM);
        return new DetailProductPage(driver);
    }
}
