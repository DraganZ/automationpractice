package pages;

import components.AbstractBaseComponent;
import components.HeaderComponent;
import components.MenuComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage extends AbstractBaseComponent {

    protected HeaderComponent headerComponent;
    protected MenuComponent menuComponent;

    public AbstractPage(WebDriver driver) {
        super(driver);
        this.wait = new WebDriverWait(driver,20);
        this.headerComponent = new HeaderComponent(driver);
        this.menuComponent = new MenuComponent(driver);
    }

    public MenuComponent getMenu() {
        return menuComponent;
    }

    public HeaderComponent getHeaderComponent() {
        return headerComponent;
    }
}
