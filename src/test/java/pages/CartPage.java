package pages;

import components.AbstractBaseComponent;
import components.CheckoutComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends AbstractBaseComponent {

    public CartPage(WebDriver driver) {
         super(driver);
    }

    private By CHECKOUT_BOX = By.cssSelector("a[title='Proceed to checkout'] span");

    public CheckoutComponent clickCheckout() {
        clickLink(CHECKOUT_BOX);
        return new CheckoutComponent(driver);
    }
}
